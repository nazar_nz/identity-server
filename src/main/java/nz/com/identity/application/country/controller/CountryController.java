package nz.com.identity.application.country.controller;

import nz.com.identity.domain.country.Country;
import nz.com.identity.domain.country.service.CountryService;
import nz.com.identity.domain.country.spec.CountryListSpec;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CountryController {

    private CountryService countryService;

    public CountryController(CountryService countryService) {
        this.countryService = countryService;
    }

    @GetMapping("/countries")
    public Page<Country> getAll(CountryListSpec spec, Pageable pageable) {
        return this.countryService.all(spec, pageable);
    }

    @PostMapping("/countries")
    public Country activateCountry(String iso2, boolean status) {
        return this.countryService.update(iso2, status);
    }
}
