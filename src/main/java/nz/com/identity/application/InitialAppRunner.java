package nz.com.identity.application;

import com.github.javafaker.Faker;
import nz.com.identity.domain.client.Client;
import nz.com.identity.domain.client.ClientService;
import nz.com.identity.domain.client.requests.ClientRequest;
import nz.com.identity.domain.country.service.CountryService;
import nz.com.identity.domain.user.service.UserService;
import nz.com.identity.domain.user.requests.UserCreateRequest;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.Locale;
import java.util.Random;

@Component
public class InitialAppRunner implements CommandLineRunner {

    private UserService userService;
    private ClientService clientService;
    private CountryService countryService;

    public InitialAppRunner(UserService userService, ClientService clientService, CountryService countryService) {
        this.userService = userService;
        this.clientService = clientService;
        this.countryService = countryService;
    }

    @Override
    public void run(String... args) throws Exception {
        Faker faker = new Faker();

        for (int i = 0; i < 100; i++ ) {

            UserCreateRequest request = new UserCreateRequest();

            request.setEmail(faker.internet().emailAddress());
            request.setPassword(faker.internet().password());
            request.setFirstName(faker.name().firstName());
            request.setLastName(faker.name().lastName());
            request.setPatronymic(faker.name().name());
            request.setCountry(faker.address().countryCode());
            request.setLanguage("ru");
            request.setAvatarUrl(faker.avatar().image());

            userService.create(request);
        }


        Random r = new Random();
        int low = 30;
        int high = 3600;

        for ( int i = 0; i < 3; i++) {
            int result = r.nextInt(high-low) + low;

            ClientRequest request = new ClientRequest(
                    faker.internet().uuid(),
                    Client.PUBLIC,
                    faker.internet().ipV4Address(),
                    "Cli application",
                    result,
                    86400,
                    null,
                    null,
                    null
            );

            clientService.create(request);
        }


        String[] countryCodes = Locale.getISOCountries();
        String[] languages = Locale.getISOLanguages();

        for (String countryCode : countryCodes) {

            Locale locale = new Locale("", countryCode);
            String iso3 = locale.getISO3Country();
            String code = locale.getCountry();
            String name = locale.getDisplayCountry();

            this.countryService.create(code, iso3, name, name, "en");
        }

    }
}
