package nz.com.identity.application.user;

import nz.com.identity.domain.user.entity.User;
import nz.com.identity.domain.user.entity.enumeration.UserStatus;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;

public class AppUser implements UserDetails {

    private User user;

    public AppUser(User user) {
        this.user = user;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    @Override
    public String getPassword() {
        return this.user.getPassword().toString();
    }

    @Override
    public String getUsername() {
        return this.user.getEmail().toString();
    }

    @Override
    public boolean isAccountNonExpired() {
        return this.user.getStatus() != UserStatus.deleted;
    }

    @Override
    public boolean isAccountNonLocked() {
        return this.user.getStatus() != UserStatus.banned;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return false;
    }

    @Override
    public boolean isEnabled() {
        return false;
    }
}
