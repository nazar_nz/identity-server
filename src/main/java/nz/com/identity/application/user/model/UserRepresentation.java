package nz.com.identity.application.user.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import nz.com.identity.domain.user.entity.User;

import java.util.Date;

public class UserRepresentation {

    private String id;
    private String uuid;
    private String email;
    private String firstName;
    private String lastName;
    private String patronymic;
    private String country;
    private String language;
    private String status;
    private String avatarUrl;

    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date createdAt;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date updatedAt;

    public static UserRepresentation createFrom(User user) {

        UserRepresentation representation = new UserRepresentation();
        representation.setId(user.getId().toString());
        representation.setUuid(user.getUuid().toString());
        representation.setEmail(user.getEmail().getEmail());
        representation.setFirstName(user.getFullName().getFirstName());
        representation.setLastName(user.getFullName().getLastName());
        representation.setPatronymic(user.getFullName().getPatronymic());
        representation.setCountry(user.getCountry().getISO2Code());
        representation.setLanguage(user.getLanguage().getLang());
        representation.setStatus(user.getStatus().name());
        representation.setCreatedAt(user.getCreatedAt());
        representation.setUpdatedAt(user.getUpdatedAt());
        representation.setAvatarUrl(user.getAvatarUrl());

        return representation;
    }

    public String getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }
}
