package nz.com.identity.application.user.controller;

import com.sun.javaws.exceptions.InvalidArgumentException;
import nz.com.identity.application.user.model.UserRepresentation;
import nz.com.identity.domain.user.service.UserService;
import nz.com.identity.domain.user.exception.EmailInvalidException;
import nz.com.identity.domain.user.exception.PasswordInvalidException;
import nz.com.identity.domain.user.exception.UserNotFoundException;
import nz.com.identity.domain.user.requests.UserCreateRequest;
import nz.com.identity.domain.user.requests.UserCredentialsRequest;
import nz.com.identity.domain.user.spec.UserListSpec;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;

@RestController
public class UserRestController {

    private UserService userService;

    public UserRestController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/users")
    Page<UserRepresentation> all(UserListSpec spec, Pageable page) {
        return userService.all(spec, page).map(UserRepresentation::createFrom);
    }

    @PostMapping("/users")
    UserRepresentation create(@RequestBody @Valid UserCreateRequest request) throws EmailInvalidException, PasswordInvalidException, InvalidArgumentException {
        return UserRepresentation.createFrom(this.userService.create(request));
    }

    @GetMapping("/users/{id}")
    UserRepresentation one(@PathVariable Long id) throws UserNotFoundException {
        return UserRepresentation.createFrom(this.userService.getById(id));
    }

    @DeleteMapping("/users/{id}")
    UserRepresentation delete(@PathVariable Long id) throws UserNotFoundException {
        return UserRepresentation.createFrom(this.userService.delete(id));
    }

    @PutMapping("/users/{id}")
    UserRepresentation update(@PathVariable Long id, @RequestBody UserCredentialsRequest request) throws UserNotFoundException, PasswordInvalidException, EmailInvalidException {
        return UserRepresentation.createFrom(this.userService.update(id, request));
    }
}
