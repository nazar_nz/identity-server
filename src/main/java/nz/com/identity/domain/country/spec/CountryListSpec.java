package nz.com.identity.domain.country.spec;

import net.kaczmarzyk.spring.data.jpa.domain.*;
import net.kaczmarzyk.spring.data.jpa.web.annotation.And;
import net.kaczmarzyk.spring.data.jpa.web.annotation.Spec;
import nz.com.identity.domain.country.Country;
import org.springframework.data.jpa.domain.Specification;

@And({
   @Spec(path = "iso2", spec = Equal.class),
   @Spec(path = "iso3", spec = Equal.class),
   @Spec(path = "name", spec = LikeIgnoreCase.class),
   @Spec(path = "nativeName", spec = LikeIgnoreCase.class),
   @Spec(path = "languages", spec = In.class),
   @Spec(path = "defaultLanguage", spec = Equal.class),
})
public interface CountryListSpec extends Specification<Country> {}
