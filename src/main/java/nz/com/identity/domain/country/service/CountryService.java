package nz.com.identity.domain.country.service;

import nz.com.identity.domain.country.Country;
import nz.com.identity.domain.country.repository.ICountryRepository;
import nz.com.identity.domain.country.spec.CountryListSpec;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class CountryService {

    private ICountryRepository repository;

    public CountryService(ICountryRepository repository) {
        this.repository = repository;
    }

    public Country create(String iso2, String iso3, String name, String nativeName, String language) {

        Country country = new Country( iso3, iso2, name, nativeName, language);
        this.repository.save(country);

        return country;
    }

    public Country update(String iso2, boolean countryStatus) {

        Country country = this.repository.findByIso2(iso2);
        country.setActive(countryStatus);

        this.repository.save(country);

        return country;
    }

    public Country getByIsoCode(String iso2) {
        return this.repository.findByIso2(iso2);
    }

    public Country delete(String iso2) {
        Country country = this.repository.findByIso2(iso2);
        this.repository.delete(country);

        return country;
    }

    public Page<Country> all(CountryListSpec spec, Pageable page) {
        return this.repository.findAll(spec, page);
    }
}
