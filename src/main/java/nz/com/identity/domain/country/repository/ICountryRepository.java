package nz.com.identity.domain.country.repository;

import nz.com.identity.domain.country.Country;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ICountryRepository extends
        PagingAndSortingRepository<Country, String>,
        JpaSpecificationExecutor<Country> {

    public Country findByIso2(String iso2);
}
