package nz.com.identity.domain.country.value;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public final class Language {

    @Column(length = 8)
    private String language;

    public Language() {
    }

    public Language(String language) {

        if (language.length() != 2) {
            throw new RuntimeException("Invalid language code");
        }
        this.language = language;
    }

    public String getLanguage() {
        return language;
    }
}
