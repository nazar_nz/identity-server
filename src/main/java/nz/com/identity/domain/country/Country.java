package nz.com.identity.domain.country;

import nz.com.identity.domain.country.value.Language;

import javax.persistence.*;
import java.util.List;

@Entity
public class Country {

    @Id
    @Column(length = 3)
    private String iso3;

    @Column(length = 2)
    private String iso2;

    @Column(length = 128)
    private String name;

    @Column(length = 128)
    private String nativeName;

    @Column
    private boolean active;

    @ElementCollection(targetClass=Language.class)
    private List<Language> languages;

    @Embedded
    private Language defaultLanguage;

    public Country() {}

    public Country(String iso3, String iso2, String name) {
        this.iso3 = iso3;
        this.iso2 = iso2;
        this.name = name;
        this.active = false;
    }

    public Country(
        String iso3,
        String iso2,
        String name,
        String nativeName,
        String language
    ) {
        this(iso3, iso2, name);
        this.nativeName = nativeName;
        this.defaultLanguage = new Language(language);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNativeName() {
        return nativeName;
    }

    public void setNativeName(String nativeName) {
        this.nativeName = nativeName;
    }

    public Language getDefaultLanguage() {
        return defaultLanguage;
    }

    public void setDefaultLanguage(Language defaultLanguage) {
        this.defaultLanguage = defaultLanguage;
    }

    public String getIso3() {
        return iso3;
    }

    public String getIso2() {
        return iso2;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
