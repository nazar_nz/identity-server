package nz.com.identity.domain.user.value;

import nz.com.identity.domain.user.exception.EmailInvalidException;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public final class Email {

    private static final int EMAIL_MIN_LENGTH = 2;
    private static final int EMAIL_MAX_LENGTH = 100;
    @Column(length = 64)
    private String email;

    public Email() {
    }

    public Email(String email) throws EmailInvalidException {

        if (email.length() < EMAIL_MIN_LENGTH || email.length() > EMAIL_MAX_LENGTH) {
            throw new EmailInvalidException("Email address have invalid length");
        }

        if (!email.contains("@")) {
            throw new EmailInvalidException("Given string not email address");
        }

        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    @Override
    public String toString() {
        return this.email;
    }
}
