package nz.com.identity.domain.user.entity.enumeration;

public enum PhoneStatus {
    deleted,
    active
}
