package nz.com.identity.domain.user.value;

import nz.com.identity.domain.user.exception.PasswordInvalidException;
import javax.persistence.Embeddable;

@Embeddable
public final class Password {

    private static final int PASSWORD_MAX_LENGTH = 16;
    private static final int PASSWORD_MIN_LENGTH = 3;

    private String password;

    public Password() {
    }

    public Password(String password) throws PasswordInvalidException {

        if (password.length() < PASSWORD_MIN_LENGTH) {
            throw new PasswordInvalidException("Password too short. Should be more that 3 characters");
        }

        if (password.length() > PASSWORD_MAX_LENGTH) {
            throw new PasswordInvalidException("Password too long. Should be less that 16 characters");
        }

        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public String toString() {
        return this.password;
    }
}
