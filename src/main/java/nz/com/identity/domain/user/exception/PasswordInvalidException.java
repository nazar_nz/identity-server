package nz.com.identity.domain.user.exception;

public class PasswordInvalidException extends Exception {
    public PasswordInvalidException(String message) {
        super(message);
    }
}
