package nz.com.identity.domain.user.entity;

import nz.com.identity.domain.common.BaseEntity;
import nz.com.identity.domain.user.entity.enumeration.UserStatus;
import nz.com.identity.domain.user.value.Country;
import nz.com.identity.domain.user.value.Language;
import nz.com.identity.domain.user.value.Email;
import nz.com.identity.domain.user.value.Name;
import nz.com.identity.domain.user.value.Password;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
public class User extends BaseEntity {

    @Column(length = 64)
    private UUID uuid;

    @Embedded
    private Email email;

    @Embedded
    private Password password;

    @Embedded
    private Name fullName;

    private UserStatus status;

    @Column()
    private Country country;

    @Embedded
    private Language language;

    @Column
    private String avatarUrl;

    @OneToMany(mappedBy = "owner")
    private List<Phone> phones = new ArrayList<Phone>();

    public User() {
    }

    public User(Email email, Password password, Name fullName) {
        this.uuid = UUID.randomUUID();
        this.email = email;
        this.password = password;
        this.fullName = fullName;
        this.status = UserStatus.active;
    }

    public User(Email email, Password password, Name fullName, UUID uuid) {
        this(email, password, fullName);
        this.uuid = uuid;
    }

    public UUID getUuid() {
        return uuid;
    }

    public Email getEmail() {
        return email;
    }

    public Password getPassword() {
        return password;
    }

    public Name getFullName() {
        return fullName;
    }

    public UserStatus getStatus() {
        return this.status;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public Password changePassword(Password newPassword)
    {
        Password oldPassword = this.password;

        this.password = newPassword;

        return oldPassword;
    }

    public Email changeEmail(Email newEmail) {
        Email email = this.email;
        this.email = newEmail;

        return email;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }
}
