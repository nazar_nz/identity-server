package nz.com.identity.domain.user.value;

import javax.persistence.Embeddable;

@Embeddable
public final class Language {

    private String lang;

    public Language() {
    }

    public Language(String language) {
        this.lang = language;
    }

    public String getLang() {
        return lang;
    }
}
