package nz.com.identity.domain.user.value;

import com.sun.javaws.exceptions.InvalidArgumentException;

import javax.persistence.Embeddable;

@Embeddable
public final class Name {

    private String firstName;

    private String lastName;

    private String patronymic;

    private String fullName;

    public Name() {
    }

    public Name(String firstName, String lastName) throws InvalidArgumentException {

        if (firstName.length() > 75 || firstName.length() < 1) {
            throw new InvalidArgumentException(new String[]{"Invalid first name"});
        }

        if (lastName.length() > 75 || lastName.length() < 1) {
            throw new InvalidArgumentException(new String[]{"Invalid last name"});
        }

        this.firstName = firstName;
        this.lastName = lastName;
        this.fullName = String.format("%s %s", this.firstName, this.lastName);
    }

    public Name(String firstName, String lastName, String patronymic) throws InvalidArgumentException {

        this(firstName, lastName);

        if (patronymic.length() > 75 || patronymic.length() < 1) {
            throw new InvalidArgumentException(new String[]{"Invalid patronymic"});
        }

        this.patronymic = patronymic;
        this.fullName = String.format("%s %s %s", this.firstName, this.lastName, this.patronymic);
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public String getFullName() {
        return fullName;
    }

    @Override
    public String toString() {
        return this.fullName;
    }
}
