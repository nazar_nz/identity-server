package nz.com.identity.domain.user.value;

import com.sun.javaws.exceptions.InvalidArgumentException;
import javax.persistence.Embeddable;

@Embeddable
public final class Country {

    private String ISO2Code;

    private String ISO3Code;

    public Country() {
    }

    public Country(String ISO2Code) throws InvalidArgumentException {

        if (ISO2Code.length() != 2) {
            throw new InvalidArgumentException(new String[]{"Invalid country code"});
        }

        this.ISO2Code = ISO2Code;
    }

    public String getISO2Code() {
        return ISO2Code;
    }
}
