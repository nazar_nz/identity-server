package nz.com.identity.domain.user.repository;

import nz.com.identity.domain.user.entity.User;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IUserRepository extends
        PagingAndSortingRepository<User, Long>,
        JpaSpecificationExecutor<User> {
}
