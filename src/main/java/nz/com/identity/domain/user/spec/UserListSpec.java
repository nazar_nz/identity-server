package nz.com.identity.domain.user.spec;

import net.kaczmarzyk.spring.data.jpa.domain.*;
import net.kaczmarzyk.spring.data.jpa.web.annotation.Conjunction;
import net.kaczmarzyk.spring.data.jpa.web.annotation.Or;
import net.kaczmarzyk.spring.data.jpa.web.annotation.Spec;
import nz.com.identity.domain.user.entity.User;
import org.springframework.data.jpa.domain.Specification;

@Conjunction( value = {
@Or({
    @Spec(path = "fullName.firstName", params = "name", spec = LikeIgnoreCase.class),
    @Spec(path = "fullName.lastName", params = "name", spec = LikeIgnoreCase.class),
    @Spec(path = "fullName.patronymic", params = "name", spec = LikeIgnoreCase.class)
    })},
and = {
    @Spec(path = "id", spec = Equal.class),
    @Spec(path = "country.ISO2Code", params = "country", spec = Equal.class),
    @Spec(path = "language.lang", params = "language", spec = Equal.class),
    @Spec(path = "email.email", params = "email", spec = Like.class),
    @Spec(path = "createdAt", params ="created[from]", spec = GreaterThanOrEqual.class),
    @Spec(path = "createdAt", params = "created[to]", spec = LessThanOrEqual.class)
})
public interface UserListSpec extends Specification<User> {}
