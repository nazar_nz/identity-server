package nz.com.identity.domain.user.service;

import com.sun.javaws.exceptions.InvalidArgumentException;
import nz.com.identity.domain.user.entity.User;
import nz.com.identity.domain.user.factory.UserFactory;
import nz.com.identity.domain.user.exception.EmailInvalidException;
import nz.com.identity.domain.user.exception.PasswordInvalidException;
import nz.com.identity.domain.user.exception.UserNotFoundException;
import nz.com.identity.domain.user.repository.IUserRepository;
import nz.com.identity.domain.user.requests.UserCreateRequest;
import nz.com.identity.domain.user.requests.UserCredentialsRequest;
import nz.com.identity.domain.user.value.Email;
import nz.com.identity.domain.user.value.Password;
import nz.com.identity.domain.user.spec.UserListSpec;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    private IUserRepository userRepository;
    private PasswordEncoder encoder;

    public UserService(IUserRepository userRepository, PasswordEncoder encoder) {
        this.userRepository = userRepository;
        this.encoder = encoder;
    }

    public Page<User> all(UserListSpec spec, Pageable page) {
        return userRepository.findAll(spec, page);
    }

    public User getById(Long id) {
        return userRepository
                .findById(id)
                .orElseThrow(UserNotFoundException::new);
    }

    public User create(UserCreateRequest request)
            throws EmailInvalidException, PasswordInvalidException, InvalidArgumentException {

        User user = UserFactory.create(
                request.getEmail(),
                request.getPassword(),
                request.getFirstName(),
                request.getLastName(),
                request.getPatronymic(),
                request.getCountry(),
                request.getLanguage(),
                request.getAvatarUrl()
        );

        this.userRepository.save(user);

        return user;
    }

    public User delete(Long id) {

        User user = userRepository.findById(id).orElseThrow(UserNotFoundException::new);

        userRepository.delete(user);

        return user;
    }

    public User update(Long id, UserCredentialsRequest request) throws PasswordInvalidException, EmailInvalidException {

        User user = userRepository.findById(id).orElseThrow(UserNotFoundException::new);
        user.changePassword(new Password(request.getPassword()));
        user.changeEmail(new Email(request.getEmail()));

        userRepository.save(user);

        return user;
    }

}
