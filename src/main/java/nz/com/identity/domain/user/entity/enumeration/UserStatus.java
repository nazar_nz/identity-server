package nz.com.identity.domain.user.entity.enumeration;

public enum UserStatus {
    active,
    deleted,
    banned
}
