package nz.com.identity.domain.user.factory;

import com.sun.javaws.exceptions.InvalidArgumentException;
import nz.com.identity.domain.user.entity.User;
import nz.com.identity.domain.user.value.Country;
import nz.com.identity.domain.user.value.Language;
import nz.com.identity.domain.user.exception.EmailInvalidException;
import nz.com.identity.domain.user.exception.PasswordInvalidException;
import nz.com.identity.domain.user.value.Email;
import nz.com.identity.domain.user.value.Name;
import nz.com.identity.domain.user.value.Password;

public class UserFactory {

    public static User create(
            String email,
            String password,
            String firstName,
            String lastName,
            String patronymic,
            String country,
            String language,
            String avatarUrl
    ) throws PasswordInvalidException,
            EmailInvalidException,
            InvalidArgumentException
    {
        User user = new User(
            new Email(email),
            new Password(password),
            new Name(firstName, lastName, patronymic)
        );

        user.setCountry(new Country(country));
        user.setLanguage(new Language(language));
        user.setAvatarUrl(avatarUrl);

        return user;
    }
}
